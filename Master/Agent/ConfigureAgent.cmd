@echo off
REM For usage, pass /?
setlocal
if defined VERBOSE_ARG (
  set VERBOSE_ARG='Continue'
) else (
  set VERBOSE_ARG='SilentlyContinue'
)

REM Unblock the files for MEF even if the policy settings don't require it.
REM VsoAgent.exe also performs the unblock, so even if configure is performed
REM using VsoAgent.exe, the unblock will not be missed. It's better to perform
REM the unblock here too in case the policy requires it to run VsoAgent.exe.
powershell.exe -ExecutionPolicy Bypass -Command "$VerbosePreference = %VERBOSE_ARG% ; Get-ChildItem -LiteralPath '%~dp0' | ForEach-Object { Write-Verbose ('Unblock: {0}' -f $_.FullName) ; $_ } | Unblock-File | Out-Null ; Get-ChildItem -Recurse -LiteralPath '%~dp0Agent' | ForEach-Object { Write-Verbose ('Unblock: {0}' -f $_.FullName) ; $_ } | Unblock-File | Out-Null"
"%~dp0Agent\VsoAgent.exe" /configure %*